/*********************************************************************
Copyright 2015 Yaroslav Belykh (aka Yamah)

This file is part of Flash Tool ROM Backup Splitter

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->centralWidget->setLayout(ui->Main_vLayout);
    ui->tab_Files->setLayout(ui->TabFiles_vLayout);
    ui->tab_Map->setLayout(ui->TabMap_vLayout);
    ui->tab_Logs->setLayout(ui->TabLog_vLayout);

    connect(ui->actionOpen_Scatter_file, SIGNAL(triggered()), this, SLOT(OpenScatter()));
    connect(ui->actionOpen_ROM_File, SIGNAL(triggered()), this, SLOT(OpenROM()));
    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(OpenAboutQt()));
    connect(ui->actionSplit_Rom, SIGNAL(triggered()), this, SLOT(splitRom()));
    connect(ui->actionSplit_Preloader, SIGNAL(triggered()), this, SLOT(splitPreloader()));
    connect(ui->actionChoose_WorkDir, SIGNAL(triggered()), this, SLOT (OpenWorkDir()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(OpenAbout()));
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(NewProject()));

    ui->PartitionMapTable->setColumnWidth(0, 24);
    ui->PartitionMapTable->setColumnWidth(1, 150);
    ui->PartitionMapTable->setColumnWidth(2, 192);
    ui->PartitionMapTable->setColumnWidth(3, 192);
    ui->PartitionMapTable->setColumnWidth(4, 240);

    ui->button_tab1_Read->setVisible(false);
    ui->le_Lenth->setVisible(false);
    ui->le_StartAddr->setVisible(false);
    ui->lbl_lenth->setVisible(false);
    ui->lbl_staddr->setVisible(false);


    NewProject();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OpenAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::OpenAbout()
{
    QMessageBox::about(this, tr("About Flash Tool ROM Backup Splitter"),
                       tr("This program is designed to split SP Flash Tool ROM backup file into partition, which can then be flash with SP Flash Tool.\n\nDespite the fact that the program split the ROM file similarly as MTKDroidTools, the author recalls that all actions you do at your own risk.\n\nThis is a Free Software and is licensed under the GNU GPL.\nAuthor: Yaroslav Belykh aka Yamah"));
}

QString MainWindow::ChoseFileDialog(QString TittleDialogName, QString extFileName, QString HomeDir)
{
    QString CurrentFile;
    CurrentFile=QFileDialog::getOpenFileName(0, TittleDialogName, HomeDir, extFileName);
    return CurrentFile;
}

void MainWindow::on_Button_tab1_Scatter_clicked()
{
    OpenScatter();
}

void MainWindow::on_Button_tab1_Rom_clicked()
{
    OpenROM();
}

void MainWindow::on_Button_tab1_WorkDir_clicked()
{
    OpenWorkDir();
}

void MainWindow::OpenScatter()
{
    QString CurrentFile;
    CurrentFile=ChoseFileDialog("Choose Scatter file", tr("Scatter Files ( *scatter*.txt );; All Files ( *.* )"));
    ui->lineEdit_tab1_Scatter->setText(CurrentFile);
    ScatterFilename=CurrentFile;
    readScatterFile(ScatterFilename);
}

void MainWindow::OpenROM()
{
    QString CurrentFile;
    CurrentFile=ChoseFileDialog("Choose ROM file", tr("ROM Files ( ROM_* );; All Files ( *.* )"));
    RomFileName=CurrentFile;
    ui->lineEdit_tab1_Rom->setText(RomFileName);
}

void MainWindow::OpenWorkDir()
{
    WorkDir=QFileDialog::getExistingDirectory(0, tr("Directory for output"), QDir::homePath());
    WorkDir=DirOpenWorkDir(WorkDir);
    ui->lineEdit_tab1_WorkDir->setText(WorkDir);
}

void MainWindow::readScatterFile(QString file)
{
    QFile scatter;
    scatter.setFileName(file);
    if (scatter.open(QIODevice::ReadOnly))
    {
        QByteArray rawDataArray;
        QStringList listData;
        rawDataArray=scatter.readAll();
        scatter.close();
        if (QString(rawDataArray).indexOf("\r\n")==-1)
            {listData=QString(rawDataArray).split("\n");}
        else
            {listData=QString(rawDataArray).split("\r\n");}
        buildPartTables(listData);
    }
    else
    {
        qDebug() << "Error open file to Read";
        QMessageBox::warning(this,QObject::tr("Error!"),tr("File is not readable."));
        return;
    }
}

void MainWindow::buildPartTables(QStringList listData)
{
    ui->textEdit->setText(tr("Reads the scatter file.\n"));
    int index;
    index=0;
    partTable.clear();
    int DeltarowCoubt;
    DeltarowCoubt=ui->PartitionMapTable->rowCount();
    if (DeltarowCoubt>0)
    {
        for (int i=0; i<DeltarowCoubt; i++)
        {ui->PartitionMapTable->removeRow(i);}
    }
    ScatterNew=true;
    for (int i=0;i<listData.size();i++)
    {
        if (listData.at(i).indexOf("- partition_index: ")!=-1)
        {
            partTable[index].PartIndex=listData.at(i).split("- partition_index: ",QString::SkipEmptyParts).last();
            partTable[index].PartName=listData.at(i+1).split("  partition_name: ",QString::SkipEmptyParts).last();
            partTable[index].FileName=listData.at(i+2).split("  file_name: ",QString::SkipEmptyParts).last();
            if (partTable[index].FileName=="NONE")
            {
                if (partTable[index].PartName=="NVRAM")
                    {partTable[index].FileName="nvram.bin";}
                else
                {
                    if (partTable[index].PartName=="NVRAM")
                        {partTable[index].FileName="nvram.bin";}
                    else
                        {partTable[index].FileName=partTable[index].PartName.toLower();}
                }
            }
            if (partTable[index].PartName=="UBOOT")
                {partTable[index].FileName="uboot.bin";}
            if (listData.at(i+3).indexOf("  is_download: ")!=-1)
                {
                    if (listData.at(i+3).split("  is_download: ",QString::SkipEmptyParts).last()=="true")
                        {partTable[index].isDownload=true;}
                    else
                        {partTable[index].isDownload=false;}
                }
            partTable[index].Type=listData.at(i+4).split("  type: ",QString::SkipEmptyParts).last();
            partTable[index].LinearStartAddr=listData.at(i+5).split("  linear_start_addr: ",QString::SkipEmptyParts).last();
            partTable[index].PhisicalStartAddr=listData.at(i+6).split("  physical_start_addr: ",QString::SkipEmptyParts).last();
            partTable[index].PartSize=listData.at(i+7).split("  partition_size: ",QString::SkipEmptyParts).last();
            partTable[index].Region=listData.at(i+8).split("  region: ",QString::SkipEmptyParts).last();
            partTable[index].Storage=listData.at(i+9).split("  storage: ",QString::SkipEmptyParts).last();
            if (listData.at(i+10).indexOf("  boundary_check: ")!=-1)
                {
                    if (listData.at(i+10).split("  boundary_check: ",QString::SkipEmptyParts).last()=="true")
                        {partTable[index].BoundaryCheck=true;}
                    else
                        {partTable[index].BoundaryCheck=false;}
                }
            if (listData.at(i+11).indexOf("  is_reserved: ")!=-1)
                {
                    if (listData.at(i+11).split("  is_reserved: ",QString::SkipEmptyParts).last()=="true")
                        {partTable[index].isReserved=true;}
                    else
                        {partTable[index].isReserved=false;}
                }
            partTable[index].OperationType=listData.at(i+12).split("  operation_type: ",QString::SkipEmptyParts).last();
            partTable[index].Reserve=listData.at(i+13).split("  reserve: ",QString::SkipEmptyParts).last();
            index++;
            i=i+13;
        }
    }
    if (index==0)
    {
        ScatterNew=false;
        for (int i=0;i<listData.size();i++)
        {
            if (listData.at(i).indexOf("PRELOADER")!=-1)
                {SimplePartTableBuild("PRELOADER", listData.at(i).split(" ",QString::SkipEmptyParts), "preloader.bin", &i, &index);}
            if (listData.at(i).indexOf("MBR")!=-1)
                {SimplePartTableBuild("MBR", listData.at(i).split(" ",QString::SkipEmptyParts), "MBR", &i, &index);}
            if (listData.at(i).indexOf("EBR1")!=-1)
                {SimplePartTableBuild("EBR1", listData.at(i).split(" ",QString::SkipEmptyParts), "EBR1", &i, &index);}
            if (listData.at(i).indexOf("EBR2")!=-1)
                {SimplePartTableBuild("EBR2", listData.at(i).split(" ",QString::SkipEmptyParts), "EBR2", &i, &index);}
            if (listData.at(i).indexOf("UBOOT")!=-1)
                {SimplePartTableBuild("UBOOT", listData.at(i).split(" ",QString::SkipEmptyParts), "uboot.bin", &i, &index);}
            if (listData.at(i).indexOf("BOOTIMG")!=-1)
                {SimplePartTableBuild("BOOTIMG", listData.at(i).split(" ",QString::SkipEmptyParts), "boot.img", &i, &index);}
            if (listData.at(i).indexOf("RECOVERY")!=-1)
                {SimplePartTableBuild("RECOVERY", listData.at(i).split(" ",QString::SkipEmptyParts), "recovery.img", &i, &index);}
            if (listData.at(i).indexOf("SEC_RO")!=-1)
                {SimplePartTableBuild("SEC_RO", listData.at(i).split(" ",QString::SkipEmptyParts), "secro.img", &i, &index);}
            if (listData.at(i).indexOf("LOGO")!=-1)
                {SimplePartTableBuild("LOGO", listData.at(i).split(" ",QString::SkipEmptyParts), "logo.bin", &i, &index);}
            if (listData.at(i).indexOf("ANDROID")!=-1)
                {SimplePartTableBuild("ANDROID", listData.at(i).split(" ",QString::SkipEmptyParts), "system.img", &i, &index);}
            if (listData.at(i).indexOf("USRDATA")!=-1)
                {SimplePartTableBuild("USRDATA", listData.at(i).split(" ",QString::SkipEmptyParts), "data.img", &i, &index);}
            if (listData.at(i).indexOf("DSP_BL")!=-1)
                {SimplePartTableBuild("DSP_BL", listData.at(i).split(" ",QString::SkipEmptyParts), "DSP_BL", &i, &index);}
            if (listData.at(i).indexOf("PRO_INFO")!=-1)
                {SimplePartTableBuild("PRO_INFO", listData.at(i).split(" ",QString::SkipEmptyParts), "pro_info", &i, &index);}
            if (listData.at(i).indexOf("NVRAM")!=-1)
                {SimplePartTableBuild("NVRAM", listData.at(i).split(" ",QString::SkipEmptyParts), "nvram.bin", &i, &index);}
            if (listData.at(i).indexOf("PROTECT_F")!=-1)
                {SimplePartTableBuild("PROTECT_F", listData.at(i).split(" ",QString::SkipEmptyParts), "protect_f", &i, &index);}
            if (listData.at(i).indexOf("PROTECT_S")!=-1)
                {SimplePartTableBuild("PROTECT_S", listData.at(i).split(" ",QString::SkipEmptyParts), "protect_s", &i, &index);}
            if (listData.at(i).indexOf("SECCFG")!=-1)
                {SimplePartTableBuild("SECCFG", listData.at(i).split(" ",QString::SkipEmptyParts), "seccfg", &i, &index);}
            if (listData.at(i).indexOf("MISC")!=-1)
                {SimplePartTableBuild("MISC", listData.at(i).split(" ",QString::SkipEmptyParts), "misc", &i, &index);}
            if (listData.at(i).indexOf("CACHE")!=-1)
                {SimplePartTableBuild("CACHE", listData.at(i).split(" ",QString::SkipEmptyParts), "cache.img", &i, &index);}
            if (listData.at(i).indexOf("EXPDB")!=-1)
                {SimplePartTableBuild("EXPDB", listData.at(i).split(" ",QString::SkipEmptyParts), "expdb", &i, &index);}
            if (listData.at(i).indexOf("BMTPOOL")!=-1)
                {SimplePartTableBuild("BMTPOOL", listData.at(i).split(" ",QString::SkipEmptyParts), "bmtpool", &i, &index);}
            if (listData.at(i).indexOf("PMT")!=-1)
                {SimplePartTableBuild("PMT", listData.at(i).split(" ",QString::SkipEmptyParts), "pmt", &i, &index);}
            if (listData.at(i).indexOf("SECCFG")!=-1)
                {SimplePartTableBuild("SECCFG", listData.at(i).split(" ",QString::SkipEmptyParts), "seccfg", &i, &index);}
        }
    }
    if (index!=0)
    {
        partCount=index;
        ui->textEdit->setText(tr("Scatter file was reading.\n"));
        for (int i=0;i<partCount;i++)
        {
            ui->PartitionMapTable->insertRow(i);
            if (ScatterNew)
            {
                ui->PartitionMapTable->setItem(i,1,new QTableWidgetItem(partTable[i].PartName));
                ui->PartitionMapTable->setItem(i,2,new QTableWidgetItem(partTable[i].LinearStartAddr));
                ui->PartitionMapTable->setItem(i,3,new QTableWidgetItem(partTable[i].PartSize));
                ui->PartitionMapTable->setItem(i,4,new QTableWidgetItem(partTable[i].FileName));
            }
            else
            {
                ui->PartitionMapTable->setItem(i, 1, new QTableWidgetItem(partTableSimple[i].PartName));
                ui->PartitionMapTable->setItem(i, 2, new QTableWidgetItem(partTableSimple[i].StartAddress));
                ui->PartitionMapTable->setItem(i, 4, new QTableWidgetItem(partTable[i].FileName));
            }
        }
        if (ScatterNew)
        {
            for (int i=4;i<14;i++)
            {
                if (listData.at(i).indexOf("platform:")!=-1)
                {
                    QString platform;
                    platform=listData.at(i).split("platform: ",QString::SkipEmptyParts).last();
                    if (platform.indexOf("MT65")!=-1)
                    {
                        mtk65=true;
                        mtk67=false;
                    }
                    if (platform.indexOf("MT67")!=-1)
                    {
                        mtk65=false;
                        mtk67=true;
                    }
                }
            }
        }
    }
    else
        {QMessageBox::critical(this,QObject::tr("Critical!"),tr("Error in scatter file"));}
}

void MainWindow::SimplePartTableBuild(QString PartName, QStringList BaseLine, QString FileName, int* i, int* index)
{
    {
        partTableSimple[*index].PartName=PartName;
        partTableSimple[*index].StartAddress=BaseLine.last();
        partTableSimple[*index].FileName=FileName;
        if (BaseLine.first().indexOf("__NODL_")==-1)
            {partTableSimple[*index].isDownload=true;}
        else
            {partTableSimple[*index].isDownload=false;}
        *i=*i+2;
        *index=*index+1;
    }
}

void MainWindow::splitRom()
{
    ui->textEdit->append(tr("Split ROM.\n"));
    WorkInProcess(false);
    ui->progressBar->setMaximum(partCount);
    if (ScatterFilename!="")
    {
        if (RomFileName!="")
        {
            if (WorkDir!="")
            {
                QFile Rom;
                Rom.setFileName(RomFileName);
                quint64 RomSize;
                RomSize=Rom.size();
                ui->textEdit->append(tr("ROM file size: ")+QVariant(RomSize).toString()+tr("bytes.\n"));
                for (int i=0;i<partCount;i++)
                {
                    if (Rom.open(QIODevice::ReadOnly))
                    {
                        QString WD;
                        WD=WorkDir;
                        QByteArray PartSource;
                        quint64 startAddr;
                        quint64 readLength;
                        QString partName;
                        if (ScatterNew)
                        {
                            startAddr=StringHexAddres2DigitalHexAddress(partTable[i].LinearStartAddr);
                            readLength=StringHexAddres2DigitalHexAddress(partTable[i].PartSize);
                            partName=partTable[i].FileName;
                        }
                        else
                        {
                            startAddr=StringHexAddres2DigitalHexAddress(partTableSimple[i].StartAddress);
                            if (i<partCount-1)
                                {readLength=StringHexAddres2DigitalHexAddress(partTableSimple[i+1].StartAddress)-StringHexAddres2DigitalHexAddress(partTableSimple[i].StartAddress);}
                            else
                                {readLength=RomSize-StringHexAddres2DigitalHexAddress(partTableSimple[i].StartAddress);}
                            partName=partTableSimple[i].FileName;
                        }
                        ui->textEdit->append(tr("Creating file ")+partName+"\n");
                        if (RomSize>startAddr)
                        {
                            if (RomSize>=startAddr+readLength)
                            {
                                QFile partImg;
                                QString OutFilename;
                                OutFilename=WD.append(QDir::separator()).append(partName);
                                partImg.setFileName(OutFilename);
                                if (partImg.exists())
                                    {partImg.remove();}
                                QString LogMess;
                                if (partImg.open(QIODevice::WriteOnly))
                                {
                                    ui->textEdit->append(tr("Reading ROM file from address ")+QVariant(startAddr).toString()+tr(" and length ") + QVariant(readLength).toString() + "\n");
                                    if (readLength<=16777216)
                                    {
                                        Rom.seek(startAddr);
                                        PartSource=Rom.read(readLength);
                                        Rom.close();
                                        if (partName=="MBR" || partName=="EBR1" || partName=="EBR2")
                                            {PartSource=DeleteZeroInEnd(PartSource);}
                                        if (partName=="preloader.bin")
                                        {
                                            PartSource=DeleteZeroInEnd(PartSource, 0);
                                            PartSource=DeleteZeroInEnd(PartSource, 255);
                                            PartSource=DeleteZeroInEnd(PartSource, 0);
                                            PartSource=DeleteZeroInEnd(PartSource, 255);
                                            //PartSource=DeleteZeroInEnd(PartSource, 0);
                                        }
                                        partImg.seek(0);
                                        partImg.write(PartSource);
                                        partImg.close();
                                        //LogMess="File ";
                                        //LogMess.append(OutFilename).append(tr(" was created.\n"));
                                    }
                                    else
                                    {
                                        quint64 partLRom;
                                        quint64 extpathLRom;
                                        quint64 curSeec;
                                        curSeec=0;
                                        partLRom=readLength/16777216;
                                        extpathLRom=readLength%16777216;
                                        for (int i=0;i<partLRom;i++)
                                        {
                                            Rom.seek(startAddr+curSeec);
                                            PartSource=Rom.read(16777216);
                                            partImg.seek(curSeec);
                                            partImg.write(PartSource);
                                            curSeec=curSeec+16777216;
                                            int m;
                                            m++;
                                        }
                                        if (extpathLRom>0)
                                        {
                                            Rom.seek(startAddr+curSeec);
                                            PartSource=Rom.read(extpathLRom);
                                            partImg.seek(curSeec);
                                            partImg.write(PartSource);
                                            int n;
                                            n++;
                                        }
                                        Rom.close();
                                        partImg.close();
                                    }
                                    LogMess=tr("File ")+OutFilename+tr(" was created.\n");
                                    ui->progressBar->setValue(i+1);
                                }
                                else
                                {
                                    qDebug() << "Error open file to Write";
                                    LogMess=tr("Error creating file ");
                                    LogMess.append(OutFilename).append(tr("."));
                                    QMessageBox::warning(this,QObject::tr("Error!"),tr("File is not writeable."));
                                    WorkInProcess(true);
                                    return;
                                }
                                ui->textEdit->append(LogMess);
                            }
                            else
                            {
                                i=partCount;
                                QMessageBox::warning(this,QObject::tr("Warning"),tr("Unexpected end of file. Perhaps your ROM file is damaged or unfinished, or you are using a scatter file from other device."));
                                ui->textEdit->append(tr("Error!\nUnexpected end of file.\n"));
                            }
                        }
                        else
                        {
                            i=partCount;
                            QMessageBox::information(this,QObject::tr("End of ROM file!"),tr("No more partitions in this ROM file. Probably, in your file was not included remaining partitions."));
                            ui->textEdit->append(tr("Warning!\nNo more partition in ROM file.\n"));
                        }
                    }
                    else
                    {
                        qDebug() << "Error open file to Read";
                        QMessageBox::information(this,QObject::tr("Error!"),tr("File is not readable."));
                        ui->textEdit->append(tr("Error!\nROM file is not readable.\n"));
                        WorkInProcess(true);
                        return;
                    }
                }
            }
            else
            {
                qDebug() << "Error directory open";
                QMessageBox::warning(this,QObject::tr("Error!"),tr("Work directory is not set."));
                WorkInProcess(true);
                return;
            }
        }
        else
        {
            qDebug() << "Error in ROM file";
            QMessageBox::warning(this,QObject::tr("Error!"),tr("ROM file is not set."));
            WorkInProcess(true);
            return;
        }
    }
    else
    {
        qDebug() << "Error scatter file";
        QMessageBox::warning(this,QObject::tr("Error!"),tr("Scatter file is not set."));
        WorkInProcess(true);
        return;
    }
    WorkInProcess();
}

void MainWindow::on_toolButton_clicked()
{
   splitRom();
}

inline quint64 MainWindow::StringHexAddres2DigitalHexAddress(QString hexStr)
{
    quint64 hex;
    bool ok;
    hex = hexStr.toLongLong(&ok, 16);
    return hex;
}

inline QByteArray MainWindow::DeleteZeroInEnd(QByteArray inpArr, char ZF)
{
    quint64 LastSign;
    QByteArray outArr;
    for (int i=0;i<inpArr.size();i++)
    {
        if (inpArr[i]!=ZF)
        {
            LastSign=i;
        }
    }
    for (int i=0;i<=LastSign;i++)
    {
        outArr[i]=inpArr[i];
    }
    return outArr;
}

inline void MainWindow::WorkInProcess(bool inW)
{
    ui->progressBar->setMinimum(0);
    ui->progressBar->setValue(0);
    ui->toolButton->setEnabled(inW);
    ui->lineEdit_tab1_Rom->setEnabled(inW);
    ui->lineEdit_tab1_Scatter->setEnabled(inW);
    ui->lineEdit_tab1_WorkDir->setEnabled(inW);
    ui->toolButton_2->setEnabled(inW);
    ui->actionOpen_ROM_File->setEnabled(inW);
    ui->actionOpen_Scatter_file->setEnabled(inW);
    ui->actionChoose_WorkDir->setEnabled(inW);
    ui->actionSplit_Preloader->setEnabled(inW);
    ui->actionSplit_Rom->setEnabled(inW);
}

void MainWindow::splitPreloader()
{
    WorkInProcess(false);
    ui->progressBar->setMaximum(2);
    if (WorkDir!="")
    {
        QString pathPreload = WorkDir;
        int ind;
        ind=findInMap("PRELOADER");
        if (ind==-1)
            {ind=findInMap("preloader");}
        if (ind>=0)
        {
            if (ScatterNew)
                {pathPreload.append("/").append(partTable[ind].FileName);}
            else
                {pathPreload.append("/").append(partTableSimple[ind].FileName);}
            QFile prldr;
            prldr.setFileName(pathPreload);
            if (prldr.exists())
            {
                if (prldr.open(QIODevice::ReadOnly))
                {
                    QByteArray PRLDR_Arr;
                    PRLDR_Arr=prldr.readAll();
                    prldr.close();
                    ui->progressBar->setValue(1);
                    QDir MTKDT;
                    QString PathMTKDT=WorkDir;
                    MTKDT.setPath(PathMTKDT.append("/").append("MTKDroidTools_Compability"));
                    if (!MTKDT.exists())
                        {MTKDT.mkpath(PathMTKDT);}
                    if (MTKDT.exists())
                    {
                        QString PreloaderMapStr;
                        PreloaderMapStr="";
                        int varC;
                        int varFil;
                        varC=0;
                        varFil=0;
                        for (int i=0;i<PRLDR_Arr.size();i++)
                        {
                            bool Excelent=false;
                            if (!mtk67)
                            {
                                if (i>15 && i+1<PRLDR_Arr.size()-16)
                                {
                                    if (PRLDR_Arr.at(i)==77 && PRLDR_Arr.at(i+1)==77 && PRLDR_Arr.at(i+2)==77 && PRLDR_Arr.at(i+3)==1 && PRLDR_Arr.at(i+4)==56 && PRLDR_Arr.at(i+5)==0 &&
                                                           PRLDR_Arr.at(i+6)==0 && PRLDR_Arr.at(i+7)==0 && PRLDR_Arr.at(i+8)==70 && PRLDR_Arr.at(i+9)==73 && PRLDR_Arr.at(i+10)==76 &&
                                                           PRLDR_Arr.at(i+11)==69 &&  PRLDR_Arr.at(i+12)==95 && PRLDR_Arr.at(i+13)==73 && PRLDR_Arr.at(i+14)==78 && PRLDR_Arr.at(i+15)==70 &&
                                            PRLDR_Arr.at(i-1)==0 && PRLDR_Arr.at(i-2)==0 && PRLDR_Arr.at(i-3)==0 && PRLDR_Arr.at(i-4)==0 && PRLDR_Arr.at(i-5)==0 && PRLDR_Arr.at(i-6)==0 &&
                                            PRLDR_Arr.at(i-7)==0 && PRLDR_Arr.at(i-8)==0 && PRLDR_Arr.at(i-9)==0 && PRLDR_Arr.at(i-10)==0 && PRLDR_Arr.at(i-11)==0 && PRLDR_Arr.at(i-12)==0 &&
                                            PRLDR_Arr.at(i-13)==0 && PRLDR_Arr.at(i-14)==0 && PRLDR_Arr.at(i-15)==0 && PRLDR_Arr.at(i-16)==0)
                                        {Excelent=true;}
                                }
                                else
                                {
                                    if (PRLDR_Arr.at(i)==77 && PRLDR_Arr.at(i+1)==77 && PRLDR_Arr.at(i+2)==77 && PRLDR_Arr.at(i+3)==1 && PRLDR_Arr.at(i+4)==56 && PRLDR_Arr.at(i+5)==0 &&
                                                           PRLDR_Arr.at(i+6)==0 && PRLDR_Arr.at(i+7)==0 && PRLDR_Arr.at(i+8)==70 && PRLDR_Arr.at(i+9)==73 && PRLDR_Arr.at(i+10)==76 &&
                                                           PRLDR_Arr.at(i+11)==69 &&  PRLDR_Arr.at(i+12)==95 && PRLDR_Arr.at(i+13)==73 && PRLDR_Arr.at(i+14)==78 && PRLDR_Arr.at(i+15)==70)
                                        {Excelent=true;}
                                }
                            }
                            if (Excelent || i+1==PRLDR_Arr.size())
                            {
                                if (i==0)
                                    {varFil=1;}
                                int Lmt;
                                if (i+1==PRLDR_Arr.size())
                                    {Lmt=i+1;}
                                else
                                    {Lmt=i;}
                                QByteArray temArr;
                                for (int j=0;j<Lmt-varC;j++)
                                {
                                    temArr[j]=PRLDR_Arr[j+varC];
                                }
                                switch (varFil)
                                {
                                    case 0:
                                    {
                                        QString FileName="emmc_boot.img";
                                        QString PreloadPartName="EMMC_BOOT";
                                        PreloaderMapStr.append(WrtPrldrSplt(PreloadPartName,temArr, PathMTKDT, FileName, DecToHex(varC)));
                                    }
                                    break;
                                    case 1:
                                    {
                                        QString FileName="preloader.bin";
                                        QString PreloadPartName="PRELOADER";
                                        PreloaderMapStr.append(WrtPrldrSplt(PreloadPartName,temArr, PathMTKDT, FileName, DecToHex(varC)));
                                    }
                                    break;
                                    default:
                                    {
                                        QString FileName="preloader_extent_";
                                        QString PreloadPartName="PRELOADER_EXTENT_";
                                        QString Str;
                                        PreloaderMapStr.append(WrtPrldrSplt(PreloadPartName.append(Str.setNum(varFil)),temArr, PathMTKDT, FileName.append(Str.setNum(varFil-2)).append(".bin"), DecToHex(varC)));
                                    }
                                    break;
                                }
                                varC=i;
                                varFil++;
                            }
                        }
                        QFile PRLDR_MAP;
                        PRLDR_MAP.setFileName(PathMTKDT.append("/preloader_map.txt"));
                        if (PRLDR_MAP.open(QIODevice::WriteOnly))
                        {
                            QTextStream out(&PRLDR_MAP);
                            out << PreloaderMapStr;
                            PRLDR_MAP.close();
                        }
                    }
                    ui->progressBar->setValue(1);
                }
                else
                {
                    qDebug() << "Error open file to Read";
                    QMessageBox::information(this,QObject::tr("Error!"),tr("File is not readable."));
                    WorkInProcess(true);
                    return;
                }
            }
        }
    }
    else
    {
        qDebug() << "Error drirectory open";
        QMessageBox::warning(this,QObject::tr("Error!"),tr("Work directory is not set."));
        WorkInProcess(true);
        return;
    }
    WorkInProcess();
}

void MainWindow::on_toolButton_2_clicked()
{
    splitPreloader();
}

inline int MainWindow::findInMap(QString str)
{
    int ind;
    ind=0;
    if (ScatterNew)
    {
        while (partTable[ind].PartName!=str && ind<partCount)
        {
            ind++;
        }
    }
    else
    {
        while (partTableSimple[ind].PartName!=str && ind<partCount)
        {
            ind++;
        }
    }
    if (ind<partCount)
        {return ind;}
    else
        {return -1;}
}

inline QString MainWindow::DecToHex(int Dec)
{
    QString Hex;
    //char buff[10];
    //sprintf(buff, "%X", dec);
    //return dec;
    Hex.setNum(Dec, 16);
    if (Hex.size()<8)
    {
        QString ZeroStr;
        ZeroStr="";
        for (int i=0;i<8-Hex.size();i++)
        {
            ZeroStr.append("0");
        }
        Hex=ZeroStr.append(Hex);
    }
    QString Addr1;
    Addr1="0x";
    Hex=Addr1.append(Hex);
    return Hex;
}

inline QString MainWindow::WrtPrldrSplt(QString PrldrPName, QByteArray tempArray, QString WD, QString fPath, QString StartAddr)
{
    QString PrldrMapStr;
    tempArray=DeleteZeroInEnd(tempArray, 0);
    tempArray=DeleteZeroInEnd(tempArray, 255);
    tempArray=DeleteZeroInEnd(tempArray, 0);
    tempArray=DeleteZeroInEnd(tempArray, 255);
    QFile FileMTKDC;
    QString Str;
    FileMTKDC.setFileName(WD.append("/").append(fPath));
    if (FileMTKDC.exists())
        {FileMTKDC.remove();}
    if (FileMTKDC.open(QIODevice::WriteOnly))
    {
        FileMTKDC.seek(0);
        FileMTKDC.write(tempArray);
        FileMTKDC.close();
        PrldrMapStr.append(PrldrPName).append("\n{\n    Start Address: ").append(StartAddr).append("\n    ");
        PrldrMapStr.append("Size: ").append(DecToHex(tempArray.size())).append("\n");
        PrldrMapStr.append("File: ").append(fPath).append("\n}\n");
    }
    return PrldrMapStr;
}

void MainWindow::on_lineEdit_tab1_Scatter_returnPressed()
{
    ScatterFilename=ui->lineEdit_tab1_Scatter->text();
    readScatterFile(ScatterFilename);
}

void MainWindow::on_lineEdit_tab1_Rom_returnPressed()
{
    RomFileName=ui->lineEdit_tab1_Rom->text();
}

void MainWindow::on_lineEdit_tab1_WorkDir_returnPressed()
{
    WorkDir=ui->lineEdit_tab1_WorkDir->text();
    WorkDir=DirOpenWorkDir(WorkDir);
    ui->lineEdit_tab1_WorkDir->setText(WorkDir);
}

inline QString MainWindow::DirOpenWorkDir(QString DirName)
{
    QDir WD;
    QFileInfoList EmtDir;
    QMessageBox::StandardButton reply=QMessageBox::No;
    WD.setPath(DirName);
    EmtDir = WD.entryInfoList(QDir::AllEntries);
    QString tempWD=DirName;
    while (EmtDir.last()!=tempWD.append("/..") && reply!=QMessageBox::Yes)
    {
        reply = QMessageBox::question(this, QObject::tr("Folder is not empty"),tr("This folder is not empty. Files in it can be removed.\nResume?"));
        if (reply != QMessageBox::Yes)
        {
            EmtDir.clear();
            WorkDir=QFileDialog::getExistingDirectory(0, tr("Directory for output"), QDir::homePath());
            WD.setPath(DirName);
            EmtDir = WD.entryInfoList(QDir::AllEntries);
        }
    }
    return DirName;
}

void MainWindow::NewProject()
{
    partCount=0;
    RomFileName="";
    ScatterFilename="";
    WorkDir="";
    partTable.clear();
    int DeltarowCoubt;
    DeltarowCoubt=ui->PartitionMapTable->rowCount();
    if (DeltarowCoubt>0)
    {
        for (int i=0; i<DeltarowCoubt; i++)
        {ui->PartitionMapTable->removeRow(i);}
    }
    ui->lineEdit_tab1_Rom->clear();
    ui->lineEdit_tab1_Scatter->clear();
    ui->lineEdit_tab1_WorkDir->clear();
}

void MainWindow::on_toolButton_3_clicked(bool checked)
{
    if (checked && RomFileName!="")
    {
        ui->button_tab1_Read->setVisible(true);
        ui->le_Lenth->setVisible(true);
        ui->le_StartAddr->setVisible(true);
        ui->lbl_lenth->setVisible(true);
        ui->lbl_staddr->setVisible(true);
    }
    else
    {
        ui->button_tab1_Read->setVisible(false);
        ui->le_Lenth->setVisible(false);
        ui->le_StartAddr->setVisible(false);
        ui->lbl_lenth->setVisible(false);
        ui->lbl_staddr->setVisible(false);
    }
}

void MainWindow::on_button_tab1_Read_clicked()
{
    quint64 startAddr;
    quint64 readLenth;
    if (ui->le_StartAddr->text().indexOf("x")!=-1)
        {startAddr=StringHexAddres2DigitalHexAddress(ui->le_StartAddr->text());}
    else
        {startAddr=ui->le_StartAddr->text().toLong();}
    if (ui->le_Lenth->text().indexOf("x")!=-1)
        {readLenth=StringHexAddres2DigitalHexAddress(ui->le_Lenth->text());}
    else
        {readLenth=ui->le_Lenth->text().toLong();}
    QFile Rom;
    Rom.setFileName(RomFileName);
    quint64 RomSize;
    RomSize=Rom.size();
    if (Rom.open(QIODevice::ReadOnly))
    {
        QString WD;
        WD=WorkDir;
        QByteArray PartSource;
        QString partName;
        QDir ReadDP;
        ReadDP.setPath(WD.append("/").append("SingledFiles"));
        if (!ReadDP.exists())
            {ReadDP.mkpath(WD);}
        if (ReadDP.exists())
        {
            if (RomSize>startAddr && RomSize>=startAddr+readLenth)
            {
                partName="Data.img";
                QFile partImg;
                partImg.setFileName(WD+"/"+partName);
                if (partImg.exists())
                    {partImg.remove();}
                if (partImg.open(QIODevice::WriteOnly))
                {
                    Rom.seek(startAddr);
                    PartSource=Rom.read(readLenth);
                    Rom.close();
                    partImg.seek(0);
                    partImg.write(PartSource);
                    partImg.close();
                }
            }
        }
    }
}

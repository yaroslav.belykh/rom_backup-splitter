<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <source>Log</source>
        <translation>Registro eventi</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nuova suddivisione</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Guida in linea</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Sfoglia</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Dimensione</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Guida in linea</translation>
    </message>
    <message>
        <source>File </source>
        <translation>File</translation>
    </message>
    <message>
        <source>Files</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Directory for output</source>
        <translation>Cartella destinazione</translation>
    </message>
    <message>
        <source>File is not readable.</source>
        <translation>Il file non è leggibile.</translation>
    </message>
    <message>
        <source>Error in scatter file</source>
        <translation>Errore nel file scatter</translation>
    </message>
    <message>
        <source> and length </source>
        <translation> e lunghezza </translation>
    </message>
    <message>
        <source>Creating file </source>
        <translation>Creazione file</translation>
    </message>
    <message>
        <source>Error!
Unexpected end of file.
</source>
        <translation>Errore!
Fine inaspettata del file.</translation>
    </message>
    <message>
        <source>File name</source>
        <translation>Nome file</translation>
    </message>
    <message>
        <source>ROM Files ( ROM_* );; All Files ( *.* )</source>
        <translation>File ROM (ROM_*);; Tutti i file (*.*)</translation>
    </message>
    <message>
        <source>Work directory is not set.</source>
        <translation>La cartella destinazione non è stata impostata.</translation>
    </message>
    <message>
        <source>This folder is not empty. Files in it can be removed.
Resume?</source>
        <translation>Questa cartella non è vuota. I file in essa possono essere eliminati. 
Recuperare?</translation>
    </message>
    <message>
        <source>Abo&amp;ut</source>
        <translation>&amp;Info sul programma</translation>
    </message>
    <message>
        <source>Scatter Files ( *scatter*.txt );; All Files ( *.* )</source>
        <translation>File scatter (*scatter*.txt);; Tutti i file (*.*)</translation>
    </message>
    <message>
        <source>Open ROM file</source>
        <translation>Apri file ROM</translation>
    </message>
    <message>
        <source>Reading ROM file from address </source>
        <translation>Lettura file ROM dall&apos;indirizzo </translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Split the ROM file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Dividi il file ROM&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Оpzioni</translation>
    </message>
    <message>
        <source>About Flash Tool ROM Backup Splitter</source>
        <translation>Info su Flash Tool ROM Backup Splitter</translation>
    </message>
    <message>
        <source>ROM file is not set.</source>
        <translation>Il file ROM non è stato impostato.</translation>
    </message>
    <message>
        <source>Error creating file </source>
        <translation>Errore nella craezione del file </translation>
    </message>
    <message>
        <source>Scatter file is not set.</source>
        <translation>File scatter non impostato.</translation>
    </message>
    <message>
        <source>Split Preloader</source>
        <translation>Dividi preloader</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Info su Qt</translation>
    </message>
    <message>
        <source>Unexpected end of file. Perhaps your ROM file is damaged or unfinished, or you are using a scatter file from other device.</source>
        <translation>Fine inaspetatta del file. Il file ROM potrebbe essere danneggiato o non completo, o si sta utilizzando lo scatter di un altro dispositivo.</translation>
    </message>
    <message>
        <source>File is not writeable.</source>
        <translation>Il file non è scrivibile.</translation>
    </message>
    <message>
        <source>No more partitions in this ROM file. Probably, in your file was not included remaining partitions.</source>
        <translation>Nessun&apos;altra partizione nel file ROM. Probabilmente nel file non sono incluse le partizioni rimanenti.</translation>
    </message>
    <message>
        <source>Start Address</source>
        <translation>Indirizzo iniziale</translation>
    </message>
    <message>
        <source>Work Dir.:</source>
        <translation>Cartella destinazione:</translation>
    </message>
    <message>
        <source>Start Address:</source>
        <translation>Indirizzo iniziale:</translation>
    </message>
    <message>
        <source>Lenght:</source>
        <translation>Lunghezza:</translation>
    </message>
    <message>
        <source>Read</source>
        <translation>Leggi</translation>
    </message>
    <message>
        <source>ROM file size: </source>
        <translation>Dim. file ROM:</translation>
    </message>
    <message>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <source>Reads the scatter file.
</source>
        <translation>Legge il file scatter.</translation>
    </message>
    <message>
        <source>Scatter file was reading.
</source>
        <translation>Lettura file scatter completata.</translation>
    </message>
    <message>
        <source>Split ROM.
</source>
        <translation>Dividi ROM.</translation>
    </message>
    <message>
        <source>bytes.
</source>
        <translation>byte.</translation>
    </message>
    <message>
        <source> was created.
</source>
        <translation>è stato creato.</translation>
    </message>
    <message>
        <source>Warning!
No more partition in ROM file.
</source>
        <translation>Attenzione!
Nessuna partizione aggiuntiva nel file ROM.</translation>
    </message>
    <message>
        <source>Error!
ROM file is not readable.
</source>
        <translation>Errroe 
Il file ROM non è leggibile.
</translation>
    </message>
    <message>
        <source>ROM_Backup Splitter</source>
        <translation>ROM Backup Splitter</translation>
    </message>
    <message>
        <source>Split Rom</source>
        <translation>Dividi ROM</translation>
    </message>
    <message>
        <source>Scatter file:</source>
        <translation>File scatter:</translation>
    </message>
    <message>
        <source>Rom_ file:</source>
        <translation>File Rom_:</translation>
    </message>
    <message>
        <source>Map of Rom_</source>
        <translation>Mappa file ROM_</translation>
    </message>
    <message>
        <source>Acctions</source>
        <translation>Azioni</translation>
    </message>
    <message>
        <source>Open Scatter file</source>
        <translation>Apri file scatter</translation>
    </message>
    <message>
        <source>Choose WorkDir</source>
        <translation>Scgeli cartella</translation>
    </message>
    <message>
        <source>This program is designed to split SP Flash Tool ROM backup file into partition, which can then be flash with SP Flash Tool.

Despite the fact that the program split the ROM file similarly as MTKDroidTools, the author recalls that all actions you do at your own risk.

This is a Free Software and is licensed under the GNU GPL.
Author: Yaroslav Belykh aka Yamah</source>
        <translation>ROM Backup Splitter v. 0.2.2b

Questio programma divide un file backup ROM di Flashtool in partizioni, che possono essere poi flashate con Flashtool.

Anche se questo programma lavora in modo simile a MTKDRoid, l&apos;autroe fa rpesente che ogni azione è fatta a tuo rischio.

Questo è un softare gratuito licenziato sotto licenza GPL.

Autiore:  Yaroslav Belykh alias Yama
</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error!</source>
        <translation>Errore!</translation>
    </message>
    <message>
        <source>Critical!</source>
        <translation>Errroe critico!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <source>End of ROM file!</source>
        <translation>Fine del file ROM!</translation>
    </message>
    <message>
        <source>Folder is not empty</source>
        <translation>La cartella non è vuota</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>ROM_Backup Splitter</source>
        <translation>ROM_Backup Splitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="73"/>
        <location filename="../mainwindow.ui" line="589"/>
        <source>Split Rom</source>
        <translation>Разбить Rom</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <location filename="../mainwindow.ui" line="594"/>
        <source>Split Preloader</source>
        <translation>Разбить прелоадер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="118"/>
        <source>Files</source>
        <translation>Файлы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="159"/>
        <source>Scatter file:</source>
        <translation>Scatter файл:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <location filename="../mainwindow.ui" line="238"/>
        <location filename="../mainwindow.ui" line="289"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="216"/>
        <source>Rom_ file:</source>
        <translation>Rom_ файл:
</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="273"/>
        <source>Work Dir.:</source>
        <translation>Рабочий каталог:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="337"/>
        <source>Start Address:</source>
        <translation>Начальный адрес:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="359"/>
        <source>Lenght:</source>
        <translation>Длина:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="381"/>
        <source>Read</source>
        <translation>Чтения</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="392"/>
        <source>Map of Rom_</source>
        <translation>Карта Rom_ файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="433"/>
        <source>Start Address</source>
        <translation>Начальный адрес</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="438"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="443"/>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="453"/>
        <source>Log</source>
        <translation>Лог</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="509"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="576"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="581"/>
        <source>Abo&amp;ut</source>
        <translation>О &amp;Программе</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="519"/>
        <source>Acctions</source>
        <translation>Действия</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Split the ROM file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Разбить ROM файл&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="526"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="532"/>
        <source>&amp;Help</source>
        <translation>По&amp;мощь</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="545"/>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="550"/>
        <source>Open ROM file</source>
        <translation>Открыть Rom файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <source>Open Scatter file</source>
        <translation>Открыть scatter файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="560"/>
        <source>Choose WorkDir</source>
        <translation>Рабочий каталог</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="100"/>
        <source>Scatter Files ( *scatter*.txt );; All Files ( *.* )</source>
        <translation>Scatter файл ( *scatter*.txt );; All Files ( *.* )</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="109"/>
        <source>ROM Files ( ROM_* );; All Files ( *.* )</source>
        <translation>ROM файл ( ROM_* );; All Files ( *.* )</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="116"/>
        <location filename="../mainwindow.cpp" line="788"/>
        <source>Directory for output</source>
        <translation>Каталог для выходных файлов</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <location filename="../mainwindow.cpp" line="463"/>
        <location filename="../mainwindow.cpp" line="663"/>
        <source>File is not readable.</source>
        <translation>Файл не доступен для чтения.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="340"/>
        <source>bytes.
</source>
        <translation>байт(а).</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="380"/>
        <source> and length </source>
        <translation> и длины </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Creating file </source>
        <translation>Создается файл </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="71"/>
        <source>About Flash Tool ROM Backup Splitter</source>
        <translation>О программе Flash Tool ROM Backup Splitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="72"/>
        <source>This program is designed to split SP Flash Tool ROM backup file into partition, which can then be flash with SP Flash Tool.

Despite the fact that the program split the ROM file similarly as MTKDroidTools, the author recalls that all actions you do at your own risk.

This is a Free Software and is licensed under the GNU GPL.
Author: Yaroslav Belykh aka Yamah</source>
        <translation>Эта программа предназначена для разделения файла SP Flastools Rom-backup на разделы, которые потом можно отдельно прошить SP FlashToos.                                       

Не смотря на то, что программа разбивает Rom-файлы аналогично MTKDroidTools, автор напоминает, что все действия Вы делаете на Ваш страх и риск.                                                                    
Это Свободное программное обеспечение, распростронняемое под лицензией GNU GPL.
Автор: ЙадоФитый ПлюсЧ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="144"/>
        <source>Reads the scatter file.
</source>
        <translation>Читаю scatter-файл.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="266"/>
        <source>Scatter file was reading.
</source>
        <translation>Scatter-файл был считан.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Error in scatter file</source>
        <translation>Ошибка в scatter-фвайле</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="327"/>
        <source>Split ROM.
</source>
        <translation>Разрезать ROM.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="340"/>
        <source>ROM file size: </source>
        <translation>Размер ROM файла:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="380"/>
        <source>Reading ROM file from address </source>
        <translation>Читаю фалй ROM с адреса</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="432"/>
        <source>File </source>
        <translation>Файл </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="432"/>
        <source> was created.
</source>
        <translation> был создан.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="438"/>
        <source>Error creating file </source>
        <translation>Ошибка создания файла </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="439"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="440"/>
        <source>File is not writeable.</source>
        <translation>Файл не доступен для записи.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="449"/>
        <source>Unexpected end of file. Perhaps your ROM file is damaged or unfinished, or you are using a scatter file from other device.</source>
        <translation>Неожиданный конец файла. Возможно ваш ROM файл поврежден или считан не до конца, или вы используете scatter-файл от другого устройства.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="450"/>
        <source>Error!
Unexpected end of file.
</source>
        <translation>Ошибка!
Неожиданный конец файла.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="456"/>
        <source>No more partitions in this ROM file. Probably, in your file was not included remaining partitions.</source>
        <translation>Нет больше разделов в этом ROM файле. Вероятно, не все разделы были ззабэкаплены.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="457"/>
        <source>Warning!
No more partition in ROM file.
</source>
        <translation>Внимание! ROM файл не содержит больше разделов.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="464"/>
        <source>Error!
ROM file is not readable.
</source>
        <translation>Ошибка!
ROM файл не возможно прочитать.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="473"/>
        <location filename="../mainwindow.cpp" line="673"/>
        <source>Work directory is not set.</source>
        <translation>Не задан рабочий каталог.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="481"/>
        <source>ROM file is not set.</source>
        <translation>ROM файл не выбран.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="489"/>
        <source>Scatter file is not set.</source>
        <translation>Scatter файл не выбран.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="784"/>
        <source>This folder is not empty. Files in it can be removed.
Resume?</source>
        <translation>Этот каталог не пустой. Файлы могут быть удалены. Продолжить?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <location filename="../mainwindow.cpp" line="440"/>
        <location filename="../mainwindow.cpp" line="463"/>
        <location filename="../mainwindow.cpp" line="473"/>
        <location filename="../mainwindow.cpp" line="481"/>
        <location filename="../mainwindow.cpp" line="489"/>
        <location filename="../mainwindow.cpp" line="663"/>
        <location filename="../mainwindow.cpp" line="673"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Critical!</source>
        <translation>Критическая ошибка!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="449"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="456"/>
        <source>End of ROM file!</source>
        <translation>Конец ROM файла!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="784"/>
        <source>Folder is not empty</source>
        <translation>Каталог не пуст</translation>
    </message>
</context>
</TS>

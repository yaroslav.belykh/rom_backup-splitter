#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QSysInfo>
#include <QCheckBox>
#include <QFile>
#include <QByteArray>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QDebug>
#include <QTranslator>
#include <QLibraryInfo>



typedef struct _partTable {
    QString PartIndex;
    QString PartName;
    QString FileName;
    bool isDownload;
    QString Type;
    QString LinearStartAddr;
    QString PhisicalStartAddr;
    QString PartSize;
    QString Region;
    QString Storage;
    bool BoundaryCheck;
    bool isReserved;
    QString OperationType;
    QString Reserve;
} TpartTable;

typedef struct _partTableSimple {
    QString PartName;
    QString FileName;
    QString StartAddress;
    bool isDownload;
} TpartTableSimple;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString ChoseFileDialog(QString, QString, QString HomeDir = QDir::homePath());
    void readScatterFile(QString);
    void buildPartTables(QStringList);
    void SimplePartTableBuild(QString, QStringList, QString, int*, int*);
    inline quint64 StringHexAddres2DigitalHexAddress(QString);
    inline QByteArray DeleteZeroInEnd(QByteArray, char ZF = 0);
    inline void WorkInProcess(bool inW = true);
    inline int findInMap(QString);
    inline QString DecToHex(int);
    inline QString WrtPrldrSplt(QString, QByteArray, QString, QString, QString);
    inline QString DirOpenWorkDir(QString);

private slots:
    void OpenROM(void);
    void OpenScatter(void);
    void OpenWorkDir(void);
    void splitRom(void);
    void splitPreloader(void);
    void NewProject(void);

    void OpenAbout(void);
    void OpenAboutQt(void);

    void on_Button_tab1_Scatter_clicked();

    void on_Button_tab1_Rom_clicked();

    void on_Button_tab1_WorkDir_clicked();

    void on_toolButton_clicked();

    void on_toolButton_2_clicked();

    void on_lineEdit_tab1_Scatter_returnPressed();

    void on_lineEdit_tab1_Rom_returnPressed();

    void on_lineEdit_tab1_WorkDir_returnPressed();

    void on_toolButton_3_clicked(bool checked);

    void on_button_tab1_Read_clicked();

private:
    Ui::MainWindow *ui;

    QMap<int, TpartTable> partTable;
    QMap<int, TpartTableSimple> partTableSimple;
    int partCount;
    bool ScatterOld;
    bool ScatterNew;

    QString RomFileName;
    QString ScatterFilename;
    QString WorkDir;

    bool mtk65;
    bool mtk67;

};

#endif // MAINWINDOW_H

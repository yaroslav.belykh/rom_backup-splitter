/*********************************************************************
Copyright 2015 Yaroslav Belykh (aka Yamah)

This file is part of Rom_ FlashTools-BackUp Splitter

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;
    //bool resultT;
    //QString trDir = "/usr/share/qshutdown/translations/";

    //THE FOLLWOING LINE WAS "../ROM" which should have been "../Rom"
    QString trDir = "translations";

#if 0 //defined(Q_OS_LINUX)
    QString TransL=QApplication::applicationDirPath();
    if (TransL=="/usr/bin")
        {trDir = "/usr/share/ROM_Backup-Splitter/translations/";}
    if (TransL=="/usr/local/bin")
        {trDir = "/usr/local/share/ROM_Backup-Splitter/translations/";}
#endif

//#if defined(Q_OS_WIN32)
    QString appDir=QApplication::applicationDirPath();
    qDebug() << "Application directory = " << appDir;

    QString locale = QLocale::system().name();
    //locale = "it_IT";
    qDebug() << "System local = " << locale;

    QString trDirAbsolute = QDir(appDir + "/" + trDir).absolutePath();
    qDebug() << "Translation relative directory = "<< trDir;
    qDebug() << "Translation absolute directory = "<< trDirAbsolute;
//#endif

    QString ProgramVersion = "0.2.1";
    QString TranslationFile = "ROM_Backup-Splitter-" + locale + ".qm";
    QString TranslationFileFullPath = trDirAbsolute + "/" + TranslationFile;

    qDebug() << "Program Version = "<< ProgramVersion;
    qDebug() << "Translation file name = "<< TranslationFile;
    qDebug() << "Translation file full path = "<< TranslationFileFullPath;

    translator.load(TranslationFile,trDirAbsolute);

    a.installTranslator(&translator);

    MainWindow w;
    w.show();

    return a.exec();
}
